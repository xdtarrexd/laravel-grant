<?php

namespace Tarre\LaravelGrant\Tests;


use Illuminate\Config\Repository;
use Illuminate\Filesystem\FilesystemServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Translation\TranslationServiceProvider;
use \PHPUnit\Framework\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected $serviceProviders = [
        TranslationServiceProvider::class,
        FilesystemServiceProvider::class,
    ];

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $app = $this->bootApp();

        $this->registerServiceProviders($app);

        parent::__construct($name, $data, $dataName);
    }

    protected function bootApp()
    {
        $app = new Application(
            $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
        );

        // bind config
        $app->instance('config', $config = new Repository([
            'laravel-grant' => []
        ]));

        return $app;
    }

    protected function registerServiceProviders($app)
    {
        foreach ($this->serviceProviders as $serviceProvider) {
            $app->register($serviceProvider);
        }
    }


}
