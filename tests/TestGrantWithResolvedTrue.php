<?php


namespace Tarre\LaravelGrant\Tests;


use Tarre\LaravelGrant\Contracts\GrantResourceContract;

class TestGrantWithResolvedTrue implements GrantResourceContract
{

    public function doResolve(): bool
    {
        return true;
    }

    public function description(): string
    {
        return 'allowed';
    }
}
