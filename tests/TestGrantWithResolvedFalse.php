<?php


namespace Tarre\LaravelGrant\Tests;


use Tarre\LaravelGrant\Contracts\GrantResourceContract;

class TestGrantWithResolvedFalse implements GrantResourceContract
{

    public function doResolve(): bool
    {
        return false;
    }

    public function description(): string
    {
        return 'denied';
    }
}
