<?php

namespace Tarre\LaravelGrant\Tests\Unit;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Tarre\LaravelGrant\Grant;
use Tarre\LaravelGrant\Tests\TestCase;

class CommonGrantCasesTest extends TestCase
{
    public function test_set_token_storage()
    {
        Grant::setTokenStorage(['a' => 'a', 'b' => 'b']);

        $this->assertSame([
            'a' => 'a',
            'b' => 'b'
        ], Grant::getValidTokens());
    }

    public function test_allow_token_only()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        Grant::only('token_a');
        $this->assertTrue(true);
    }

    public function test_deny_token_only()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        try {
            Grant::only('token_b');
        } catch (AuthorizationException $exception) {
            $this->assertTrue(true);
        } catch (Exception $exception) {
            $this->assertTrue(false);
        }
    }

    public function test_allow_token_but()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        Grant::but('token_b');
        $this->assertTrue(true);
    }

    public function test_deny_token_but()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        try {
            Grant::but('token_a');
        } catch (AuthorizationException $exception) {
            $this->assertTrue(true);
        } catch (Exception $exception) {
            $this->assertTrue(false);
        }
    }

    public function test_allow_token_or()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        Grant::or(['token_a', 'token_b']);
        $this->assertTrue(true);
    }

    public function test_deny_token_or()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
                'token_c' => 'description c'
            ],
            [
                'token_a'
            ]);

        try {
            Grant::or(['token_b', 'token_c']);
        } catch (AuthorizationException $exception) {
            $this->assertTrue(true);
        } catch (Exception $exception) {
            $this->assertTrue(false);
        }
    }

}
