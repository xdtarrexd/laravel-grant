<?php


namespace Tarre\LaravelGrant\Tests\Unit;


use Illuminate\Auth\Access\AuthorizationException;
use Tarre\LaravelGrant\Grant;
use Tarre\LaravelGrant\Tests\TestCase;
use Tarre\LaravelGrant\Tests\TestGrantWithResolvedFalse;
use Tarre\LaravelGrant\Tests\TestGrantWithResolvedTrue;

class GrantResourceTest extends TestCase
{
    public function test_allow_grant()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        Grant::only(new TestGrantWithResolvedTrue);
        $this->assertTrue(true);
    }

    public function test_allow_grant_mixed()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        Grant::or(['token_b', new TestGrantWithResolvedTrue]);
        $this->assertTrue(true);
    }

    public function test_deny_grant()
    {
        Grant::mock(
            [
                'token_a' => 'description a',
                'token_b' => 'description b',
            ],
            [
                'token_a'
            ]);

        try {
            Grant::only(new TestGrantWithResolvedFalse);
        } catch (AuthorizationException $exception) {
            $this->assertTrue(true);
        } catch (\Exception $exception) {
            $this->assertTrue(false);
        }
    }
}
