<?php


namespace Tarre\LaravelGrant\Contracts;


interface GrantResourceContract
{
    public function doResolve(): bool;

    public function description(): string;
}
