<?php


namespace Tarre\LaravelGrant\Abstracts;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Tarre\LaravelGrant\Contracts\GrantResourceContract;

abstract class GrantResource implements GrantResourceContract
{
    protected $model;

    /**
     * GrantResource constructor.
     * @param Model|mixed $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return bool
     */
    public function doResolve(): bool
    {
        return $this->resolve(Auth::User(), $this->model);
    }
}
