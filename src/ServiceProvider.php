<?php


namespace Tarre\LaravelGrant;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Tarre\LaravelGrant\Commands\MakeGrantConsole;


class ServiceProvider extends BaseServiceProvider
{

    public function boot()
    {
        // Load trans files
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'lang', 'laravel-grant');

        /*
         * Publish config
         */
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'config.php' => config_path('laravel-grant.php'),
            __DIR__ . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'lang' => resource_path('lang' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'laravel-grant'),
        ], 'laravel-grant');

    }

    public function register()
    {
        $this->commands([
            MakeGrantConsole::class
        ]);
    }


}
