<?php


namespace Tarre\LaravelGrant;


use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\Auth;
use Tarre\LaravelGrant\Contracts\GrantResourceContract;
use Tarre\LaravelGrant\Events\UserDeniedEvent;
use Tarre\LaravelGrant\Events\UserGrantedEvent;
use Tarre\LaravelGrant\Exceptions\AuthUserNotSetException;
use Tarre\LaravelGrant\Exceptions\InvalidTokenException;

final class Grant
{
    /**
     * @return array
     */
    protected static $tokens;
    protected static $userTokens;

    /**
     * @param $path
     * @param null $default
     * @return mixed
     * @throws BindingResolutionException
     */
    protected static function config($path, $default = null)
    {
        return app()->make('config')->get($path, $default);
    }

    /**
     * @return bool
     * @throws BindingResolutionException
     */
    protected static function shouldDispatchEvents(): bool
    {
        return self::config('laravel-grant.dispatch_events', false);
    }

    /**
     * @return array
     * @throws AuthUserNotSetException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    protected static function getUserTokens()
    {
        if (!self::$userTokens) {

            if (!$user = Auth::user()) {
                throw new AuthUserNotSetException;
            }

            $userTokenKey = self::config('laravel-grant.user_token_key');

            self::$userTokens = Auth::user()->{$userTokenKey};

            if (is_object(self::$userTokens) && self::$userTokens instanceof Arrayable) {
                self::$userTokens = self::$userTokens->toArray();
            }

            // also validate them
            self::validateTokens(self::$userTokens);
        }

        return self::$userTokens;
    }

    /**
     * @param $givenTokens
     * @return array
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    protected static function validateTokens($givenTokens)
    {
        if (!is_array($givenTokens)) {
            $givenTokens = [$givenTokens];
        }

        $validTokenKeys = array_keys(self::getValidTokens());

        foreach ($givenTokens as $givenToken) {

            // Don't validate grant resources
            if (is_object($givenToken) && $givenToken instanceof GrantResourceContract) {
                continue;
            }

            $givenToken = self::resolveTokenName($givenToken);

            if (!in_array($givenToken, $validTokenKeys)) {
                throw new InvalidTokenException(trans('laravel-grant::messages.invalid_token', [
                    'givenToken' => $givenToken
                ]));
            }
        }

        return $givenTokens;
    }

    /**
     * Deny the request, throw error and dispatch optional events
     * @param array $requiredTokens
     * @param string $glue
     * @throws AuthUserNotSetException
     * @throws AuthorizationException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    protected static function deny(array $requiredTokens, $glue = 'and')
    {
        $userTokens = self::getUserTokens();

        // mapper for array_map
        $nameMapper = function ($token) {
            $token = self::resolveTokenName($token);
            $tokenDescription = self::resolveTokenDescription($token);
            return "\"$tokenDescription\"";
        };

        // get the description of the require tokens
        $requiredTokenNames = array_map($nameMapper, $requiredTokens);

        // get the description of the user tokens
        $userTokenNames = array_map($nameMapper, $userTokens);

        // trans condition
        $condition = trans(sprintf('laravel-grant::messages.must%s', $glue == 'not' ? '_not' : ''));

        // trans glue
        $glue = trans("laravel-grant::messages.$glue");

        // crate message to throw
        $message = trans('laravel-grant::messages.unauthorized', [
            'givenTokens' => implode(" $glue ", $requiredTokenNames),
            'providedTokens' => implode(', ', $userTokenNames),
            'condition' => $condition
        ]);

        // dispatch event
        if (self::shouldDispatchEvents()) {
            event(new UserDeniedEvent($requiredTokens, $userTokens));
        }

        // throw laravel auth exception
        throw new AuthorizationException($message);
    }

    /**
     * Allow the request
     *
     * @param array $requiredTokens
     * @throws AuthUserNotSetException|InvalidTokenException
     * @throws BindingResolutionException
     */
    protected static function allow(array $requiredTokens)
    {
        if (self::shouldDispatchEvents()) {
            $userTokens = self::getUserTokens();
            event(new UserGrantedEvent($requiredTokens, $userTokens));
        }
    }

    /**
     * @param $requiredToken
     * @return bool
     * @throws AuthUserNotSetException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    protected static function resolveTokenValue($requiredToken)
    {
        $userTokens = self::getUserTokens();

        if (is_object($requiredToken) && $requiredToken instanceof GrantResourceContract) {
            return $requiredToken->doResolve();
        } elseif (is_string($requiredToken)) {
            return in_array($requiredToken, $userTokens);
        }

        return false;
    }

    /**
     * @param $requiredToken
     * @return false|string
     */
    protected static function resolveTokenName($requiredToken)
    {
        if (is_object($requiredToken) && $requiredToken instanceof GrantResourceContract) {
            return get_class($requiredToken);
        }
        return $requiredToken;
    }

    /**
     * @param $requiredToken
     * @return false|string
     */
    protected static function resolveTokenDescription($requiredToken)
    {
        if (is_object($requiredToken) && $requiredToken instanceof GrantResourceContract) {
            return $requiredToken->description();
        }
        return $requiredToken;
    }

    /**
     * @param $requiredTokens
     * @param bool $invertMatch
     * @return bool
     * @throws AuthUserNotSetException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    protected static function hitOrMiss(&$requiredTokens, $invertMatch = true)
    {
        // validate given token names
        $requiredTokens = self::validateTokens($requiredTokens);

        if ($invertMatch) {
            foreach ($requiredTokens as $requiredToken) {
                if (!self::resolveTokenValue($requiredToken)) {
                    return true;
                }
            }
        } else {
            foreach ($requiredTokens as $requiredToken) {
                if (self::resolveTokenValue($requiredToken)) {
                    return true;
                }
            }
        }


        return false;
    }

    /**
     * Mock user tokens and token storage for testz
     *
     * @param array $tokens
     * @param array $userTokens
     * @return static
     */
    public static function mock(array $tokens, array $userTokens): self
    {
        self::$tokens = $tokens;
        self::$userTokens = $userTokens;
        return new static;
    }

    /**
     * @return array
     * Get token storage
     * @throws BindingResolutionException
     */
    public static function getValidTokens(): array
    {
        if (!self::$tokens) {
            self::$tokens = self::config('laravel-grant.available_tokens', []);
        }
        return self::$tokens;
    }

    /**
     * Change the default storage from config/laravel-grant.available_tokens
     *
     * @param array $tokens
     */
    public static function setTokenStorage(array $tokens)
    {
        self::$tokens = $tokens;
    }

    /**
     * All tokens are required
     *
     * @param $requiredTokens
     * @throws AuthorizationException
     * @throws AuthUserNotSetException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    public static function only($requiredTokens)
    {
        $allow = !self::hitOrMiss($requiredTokens, true);

        if ($allow) {
            self::allow($requiredTokens);
        } else {
            self::deny($requiredTokens);
        }
    }

    /**
     * All but the given tokens are required
     *
     * @param $requiredTokens
     * @throws AuthorizationException
     * @throws AuthUserNotSetException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    public static function but($requiredTokens)
    {
        $allow = !self::hitOrMiss($requiredTokens, false);

        if ($allow) {
            self::allow($requiredTokens);
        } else {
            self::deny($requiredTokens, 'not');
        }
    }

    /**
     * At least one one of the tokens is required
     *
     * @param $requiredTokens
     * @throws AuthUserNotSetException
     * @throws AuthorizationException
     * @throws InvalidTokenException
     * @throws BindingResolutionException
     */
    public static function or($requiredTokens)
    {
        $allow = self::hitOrMiss($requiredTokens, false);

        if ($allow) {
            self::allow($requiredTokens);
        } else {
            self::deny($requiredTokens, 'or');
        }
    }
}
