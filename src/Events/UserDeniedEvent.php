<?php


namespace Tarre\LaravelGrant\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Support\Facades\Auth;

/**
 * @property string $name
 * @property string $id
 * @property array $requiredTokens
 * @property array $givenTokens
 */
class UserDeniedEvent
{
    public $name;
    public $id;
    public $requiredTokens;
    public $givenTokens;

    public function __construct(array $requiredTokens, array $givenTokens)
    {
        $user = Auth::user();
        $this->name = $user->getAuthIdentifierName();
        $this->id = $user->getAuthIdentifier();
        $this->requiredTokens = $requiredTokens;
        $this->givenTokens = $givenTokens;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('laravel-grant');
    }

    /**
     *
     * Define event name
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'user-denied';
    }

    /**
     * @return boolean
     */
    public function broadcastWhen()
    {
        return config('laravel-grant.broadcast_events', false);
    }

}
