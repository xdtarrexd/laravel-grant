<?php


namespace Tarre\LaravelGrant\Commands;


use Illuminate\Console\Command;
use Tarre\LaravelGrant\Exceptions\GrantFileAlreadyExistsException;

class MakeGrantConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:grant {grantName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new custom Laravel Grant class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $grantName = $this->argument('grantName');

        try {
            $this->createFromStub($grantName);
            $this->info('Grant successfully created.');
        } catch (GrantFileAlreadyExistsException $exception) {
            $this->warn($exception->getMessage());
        }

        return 0;
    }

    protected function createGrantDirectoryIfNeeded()
    {
        if (!file_exists(app_path('Grants'))) {
            mkdir(app_path('Grants'));
        }
    }

    protected function createFromStub($name)
    {
        $this->createGrantDirectoryIfNeeded();

        $filePath = $this->getGrantNewPath($name);

        if (file_exists($filePath)) {
            throw new GrantFileAlreadyExistsException(sprintf('The file "%s" already exists', $filePath));
        }

        $content = $this->getGrantContents($name);

        file_put_contents($filePath, $content);
    }

    protected function getGrantContents($name)
    {
        return strtr($this->getGrantStub(), [
            '%name%' => $name
        ]);
    }

    protected function getGrantNewPath($name)
    {
        return app_path('Grants') . DIRECTORY_SEPARATOR . $name . '.php';
    }

    protected function getGrantStub()
    {
        return file_get_contents($this->getStubPath());
    }

    protected function getStubPath()
    {
        return dirname(__DIR__) . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'GrantResource.stub';
    }
}

