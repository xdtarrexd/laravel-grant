<?php

return [
    'invalid_token' => 'The token ":givenToken" does not exist in the app config.',
    'unauthorized' => 'This action is unauthorized. You :condition have access to :givenTokens to continue. :providedTokens was provided',
    'and' => 'and',
    'or' => 'or',
    'not' => 'and not',
    'must_not' => 'must not',
    'must' => 'must'
];
