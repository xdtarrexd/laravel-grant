<?php

return [
    /*******
     * Define tokens for this application
     */
    'available_tokens' => [
        // tokenName => description
        // 'news' => 'News',
        // 'sysadmin' => 'System administrator',
        // 'sysmod' => 'System moderator'
    ],

    /*******
     * Determine where the user has their tokens stored (ie: Auth::user()->access)
     */
    'user_token_key' => 'access',

    /*******
     * Event and broadcasting
     */
    'dispatch_events' => false,
    'broadcast_events' => false
];
